import { Component} from '@angular/core';

import { MusicService } from './music.service';
import { PlayerService } from '../player/player.service';
import { SearchService } from '../search/search.service';
import { AppComponent } from '../app.component';

@Component({
  moduleId: module.id,
  selector: 'music-list',
  templateUrl: './music-list.component.html' 
})

export class MusicListComponent {
  
    musicTracks: Object[] = null;
    errorMessage = null;

    constructor(
        private appComponent: AppComponent, 
        private musicService: MusicService,  
        private playerService: PlayerService,
        private searchService: SearchService) 
        { 
           this.searchService.getStream().subscribe(
            (val) => { 
                    console.log("MusicListComponent.SearchService new Filter=", val);
                    this.getMusic();  },
            (err) => { console.log("MusicListComponent.SearchService.error()", err) },
            ()    => { console.log("MusicListComponent.SearchService.completed") }
            );
        }

    getMusic()
    {
        console.log("MusicListComponent.searchService.getSearchFilter()="+ this.searchService.getSearchFilter());
        console.log("MusicListComponent.searchService.hasSearchFilter()="+ this.searchService.hasSearchFilter());
        this.musicTracks= null;
        this.errorMessage = null;
        if( this.searchService.hasSearchFilter() === true)
        {
            this.musicService.getMusic(
                this.searchService.getSearchFilter(),
                this.searchService.getSearchFilterMediaMusic(),
                this.searchService.getSearchFilterMediaVideo()
            ).subscribe(
                        music => this.musicTracks = music,
                        error =>  this.errorMessage = <any>error);
        }
    }
}