import {Injectable} from '@angular/core';
import {Http,Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class MusicService {
    /*
    media: song, movie, podcast, song, musicVideo, audiobook, shortFilm, tvShow, software, ebook, all
    */
    searchTerm = "?term=";
    searchMediaType = "&entity=";
    searchLimit = "&limit=25";
    url = 'https://itunes.apple.com/search';
    
    constructor(private http: Http) {}

    getMusic(artist: string, mediaTypeMusic:boolean, mediaTypeVideo:boolean): Observable<Object[]> {
        
        console.log("MusicService.getMusic().artist: "+ artist);
        console.log("MusicService.getMusic().mediaTypeMusic: "+ mediaTypeMusic);
        console.log("MusicService.getMusic().mediaTypeVideo: "+ mediaTypeVideo);

        let mediaType = "all";
        if( mediaTypeMusic === true)
        {
            mediaType = "song";
        }
        else if( mediaTypeVideo === true)
        {
            mediaType = "musicVideo";
        }
        console.log("MusicService.getMusic().mediaType: "+ mediaType);

        let requestURL:string = this.url+
            this.searchTerm+artist +
            this.searchLimit+
            this.searchMediaType+mediaType;
            
        console.log("MusicService.getMusic(): "+ requestURL);

        return this.http.get(requestURL)
                        .map(this.extractData)
                        .catch(this.handleError);
    }

    private extractData(res: Response) {
        console.log("MusicService.extractData(): "+res.toString() );
        let body = res.json();
        return body.results || { };
    }
    
    private handleError (error: Response | any) {
        console.log("MusicService.handleError(): "+error.toString() );
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}