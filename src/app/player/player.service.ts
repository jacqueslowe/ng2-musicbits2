import {Injectable} from '@angular/core';

@Injectable()
export class PlayerService {
    player = new Audio();
    selectedTrack;
    playerState = "stopped";    

    play(item : any)
    {
        console.log("PlayerService.play().item.previewUrl" + JSON.stringify(item.previewUrl) );
        if( this.selectedTrack == null || this.selectedTrack.trackId != item.trackId)
        {
            this.selectedTrack = item;
            this.playerState = "stopped";
        }

        if( this.playerState === "stopped")
        {
            console.log("PlayerService.play().stopped()");
            
            this.playerState = "playing";
            this.player.src = item.previewUrl;
            this.player.play();
        }
        else if( this.playerState === "playing")
        {
            console.log("PlayerService.play().pause()");
            this.player.pause();
            this.playerState = "paused";
        }
        else
        {
            console.log("PlayerService.play().play()");
            this.playerState = "playing";
            this.player.src = item.previewUrl;
            this.player.play();
        }
    }
}