import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { MusicService } from './music/music.service';
import { PlayerService } from './player/player.service';
import { SearchService } from './search/search.service';
import { MusicListComponent } from './music/music-list.component';
import { SearchComponent } from './search/search.component';

@NgModule({
  declarations: [AppComponent, MusicListComponent, SearchComponent],
  imports: [ BrowserModule, ReactiveFormsModule, HttpModule],
  providers: [MusicService, PlayerService, SearchService], 
  bootstrap: [AppComponent]
})
export class AppModule { }
