import { Component, Input } from '@angular/core';

import { MusicService } from '../music/music.service';
import { PlayerService } from '../player/player.service';
import { SearchService } from './search.service';
import { AppComponent } from '../app.component';

@Component({
  moduleId: module.id,
  selector: 'search',
  templateUrl: './search.component.html'
})

export class SearchComponent {
    @Input()  
    searchValue:string='george michael';
    mediaTypeMusic:boolean=true;
    mediaTypeVideo:boolean=false;

    constructor(
        private appComponent: AppComponent, 
        private musicService: MusicService,
        private playerService: PlayerService,  
        private searchService: SearchService) 
        { 
        }

    clearSearch() { 
        console.log("SearchComponent.clearSearch()");
        this.searchValue = '';
        this.searchService.setSearchFilter(
            this.searchValue, 
            this.mediaTypeMusic,
            this.mediaTypeVideo
            );
        this.playerService.player.pause(); 
    }

    onKey(event: any) { 
        console.log("SearchComponent.onKey(): "+this.searchValue );
        this.searchValue = event.target.value;
    }

    mediaTypeMusicChanged(event:any)
    {
        var target = event.target;
        /*
            var target = evt.target;
            if (target.checked) {
                doSelected(target);
                this._prevSelected = target;
            } else {
                doUnSelected(this._prevSelected)
            }
        */
        console.log("SearchComponent.mediaTypeMusicChanged(): "+event.target.checked);
        if( this.mediaTypeMusic === true)
        {
            this.mediaTypeVideo = false;
        }
    }
    mediaTypeVideoChanged()
    {
        console.log("SearchComponent.mediaTypeMusicChanged(): "+this.mediaTypeVideoChanged );
        if( this.mediaTypeVideo === true)
        {
            this.mediaTypeMusic = false;
        }
    }
    searchClicked() {
        console.log("SearchComponent.searchClicked().searchValue: "+this.searchValue );
        console.log("SearchComponent.searchClicked().mediaTypeMusic: "+this.mediaTypeMusic );
        console.log("SearchComponent.searchClicked().mediaTypeVideo: "+this.mediaTypeVideo );
        this.searchService.setSearchFilter(
            this.searchValue, 
            this.mediaTypeMusic,
            this.mediaTypeVideo);
        this.playerService.player.pause();
    }
}