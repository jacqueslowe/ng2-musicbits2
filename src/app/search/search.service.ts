import {Injectable} from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class SearchService {
    
    private searchSubject: Subject<string>;
    private stream$: Observable<string>;
    private searchFilter:string = null;
    private searchMediaMusic = true;
    private searchMediaVideo = false;

    constructor() {
        this.searchSubject = new Subject();
        this.stream$ = this.searchSubject.map(x=>x);
    }

    getSearchFilter() { return this.searchFilter; }
    getSearchFilterMediaMusic() { return this.searchMediaMusic; }
    getSearchFilterMediaVideo() { return this.searchMediaVideo; }
    setSearchFilter(filter:string, filterMusic:boolean, filterVideo:boolean) 
    { 
        this.searchFilter=filter; 
        this.searchMediaMusic = filterMusic;
        this.searchMediaVideo = filterVideo;
        this.searchChanged(this.searchFilter);
    }

    hasSearchFilter() { return (this.searchFilter != null && this.searchFilter.length > 0 ) ? true : false }

    getStream()
    {
        return this.stream$;
    }
    private searchChanged(newFilter:string)
    {
        this.searchSubject.next(newFilter);
    }
}