import { Component} from '@angular/core';
import { Observable } from 'rxjs/Observable';

//app
import { SearchService } from './search/search.service';
import { MusicService } from './music/music.service';
import { PlayerService } from './player/player.service';
import { MusicListComponent } from './music/music-list.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html', 
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  constructor( 
                private musicService: MusicService, //retrived data from itunes
                private searchService: SearchService, //provides a mechanism for renering and retrived search criteria
                private playerService: PlayerService //give a music track, plays/pause current track
                ) {}  
}
